# -*- coding: latin-1 -*-
import sys
import os
import vlc
import time
import random
import re
from PyQt4 import QtCore, QtGui
from PyQt4.QtGui import *
import threading

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

thread_lock = threading.Lock()

def thread_call(func, *args, **kwargs):
    thrd = threading.Thread(target = func, args = args, kwargs = kwargs)
    thrd.start()

def loop_till_sign_valid(func, loop_sign):
    init_loop_sign = loop_sign
    while loop_sign == init_loop_sign:
        loop_sign = func(loop_sign = loop_sign)
    print('loop ended on %s' % func.__name__)

def time_convert(ms_time):
    sec_time = ms_time / 1000.0
    min = int(sec_time // 60)
    sec = int((sec_time / 60.0 - min) * 60)
    return '{:0>2}:{:0>2}'.format(min, sec)

class MyDict(dict):
    def __getitem__(self, x):
        try:
            return super(MyDict, self).__getitem__(x)
        except KeyError as e:
            super(MyDict, self).__setitem__(x, MyDict())
            return super(MyDict, self).__getitem__(x)

class MyListWidget(QtGui.QListWidget):
    def dropEvent(self, event):
        event.setDropAction(QtCore.Qt.MoveAction)
        super(MyListWidget, self).dropEvent(event)
        controller.drop_listwidget_item(self.get_urls(event))

    def get_urls(self, event):
        for url in event.mimeData().urls():
            yield unicode(url.toLocalFile())

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls:
            event.accept()
        else:
            event.ignore()

    def dragMoveEvent(self, event):
        if event.mimeData().hasUrls:
            event.setDropAction(QtCore.Qt.CopyAction)
            event.accept()
        else:
            event.ignore()

class MyTreeView(QtGui.QTreeView):
    def mousePressEvent(self, event, *args, **kwargs):
        super(MyTreeView, self).mousePressEvent(event,  *args, **kwargs)
        if event.button() == QtCore.Qt.MidButton:
            model_index = self.indexAt(event.pos())
            controller.mid_click_on_tree_view_item(model_index)

class MySlider(QtGui.QSlider):
    def mousePressEvent(self, event):
        self.set_position_accurately(event.x())
        controller.position_changed()

    def mouseMoveEvent(self, event):
        self.set_position_accurately(event.x())
        controller.position_changed()

    def set_position_accurately(self, x_pos):
        self.setValue(QtGui.QStyle.sliderValueFromPosition(self.minimum(), self.maximum(), x_pos, self.width()))

class UiMainWindow(QtGui.QMainWindow):
    def __init__(self, *args, **kwargs):
        super(UiMainWindow, self).__init__(*args, **kwargs)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setup_ui()
        self.retranslate_ui()
        #QtCore.QMetaObject.connectSlotsByName(self)

    def setup_ui(self):
        self.setObjectName(_fromUtf8("self"))
        self.setFixedSize(852, 613)
        self.centralwidget = QtGui.QWidget(self)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))

        self.tree_view = MyTreeView(self.centralwidget)
        self.tree_view.setGeometry(QtCore.QRect(10, 10, 451, 520))
        self.tree_view.setObjectName(_fromUtf8("tree_view"))
        self.set_tree_view_model(self.tree_view, Business.music_path)

        self.search_line = QLineEdit(self.centralwidget)
        self.search_line.setGeometry(QtCore.QRect(10, 540, 451, 21))
        self.search_line.setObjectName(_fromUtf8("search_line"))
        self.search_line.setFocus()
   
        self.list_widget = MyListWidget(self.centralwidget)
        self.list_widget.setGeometry(QtCore.QRect(470, 260, 371, 301))
        self.list_widget.setObjectName(_fromUtf8("list_widget"))

        QtGui.QListWidget.setSelectionMode(self.list_widget, QtGui.QAbstractItemView.ExtendedSelection)
        QtGui.QListWidget.setDragDropMode(self.list_widget, QtGui.QAbstractItemView.DragDrop)
        self.list_widget.setAcceptDrops(True)

        self.lbl_played_song = QtGui.QLabel(self.centralwidget)
        self.lbl_played_song.setGeometry(QtCore.QRect(470, 15, 360, 40))
        self.lbl_played_song.setObjectName(_fromUtf8("lbl_played_song"))
        self.lbl_played_song.setText('No song is being played')
        self.lbl_played_song.setFont(QtGui.QFont("Tahoma", 10, QtGui.QFont.Bold))

        self.btn_play = QtGui.QPushButton(self.centralwidget)
        self.btn_play.setGeometry(QtCore.QRect(470, 70, 51, 51))
        self.btn_play.setAutoFillBackground(False)
        self.btn_play.setObjectName(_fromUtf8("btn_play"))
        self.position_slider = MySlider(self.centralwidget)
        self.position_slider.setGeometry(QtCore.QRect(470, 200, 371, 22))
        self.position_slider.setOrientation(QtCore.Qt.Horizontal)
        self.position_slider.setObjectName(_fromUtf8("position_slider"))
        self.position_slider.setTickInterval(0)
        self.position_slider.setSingleStep(0.5)

        self.lbl_media_time = QtGui.QLabel(self.centralwidget)
        self.lbl_media_time.setGeometry(QtCore.QRect(470, 220, 40, 30))
        self.lbl_media_time.setObjectName(_fromUtf8("lbl_media_time"))
        # self.lbl_media_time.setText('')
        self.lbl_media_time.setFont(QtGui.QFont("Tahoma", 10))

        self.lbl_media_time_length = QtGui.QLabel(self.centralwidget)
        self.lbl_media_time_length.setGeometry(QtCore.QRect(805, 220, 40, 30))
        self.lbl_media_time_length.setObjectName(_fromUtf8("lbl_media_time_length"))
        self.lbl_media_time_length.setFont(QtGui.QFont("Tahoma", 10))

        self.btn_prev = QtGui.QPushButton(self.centralwidget)
        self.btn_prev.setGeometry(QtCore.QRect(540, 70, 51, 51))
        self.btn_prev.setObjectName(_fromUtf8("btn_prev"))
        self.btn_stop = QtGui.QPushButton(self.centralwidget)
        self.btn_stop.setGeometry(QtCore.QRect(610, 70, 51, 51))
        self.btn_stop.setObjectName(_fromUtf8("btn_stop"))
        self.btn_next = QtGui.QPushButton(self.centralwidget)
        self.btn_next.setGeometry(QtCore.QRect(680, 70, 51, 51))
        self.btn_next.setObjectName(_fromUtf8("btn_next"))
        self.btn_shuffle = QtGui.QPushButton(self.centralwidget)
        self.btn_shuffle.setGeometry(QtCore.QRect(750, 70, 51, 51))
        self.btn_shuffle.setObjectName(_fromUtf8("btn_shuffle"))
        self.setCentralWidget(self.centralwidget)
        self.volume_slider = QtGui.QSlider(self.centralwidget)
        self.volume_slider.setGeometry(QtCore.QRect(820, 70, 22, 101))
        self.volume_slider.setOrientation(QtCore.Qt.Vertical)
        self.volume_slider.setObjectName(_fromUtf8("volume_slider"))
        self.menubar = QtGui.QMenuBar(self)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 852, 21))
        self.menubar.setObjectName(_fromUtf8("menubar"))
        self.menu_file = QtGui.QMenu(self.menubar)
        self.menu_file.setObjectName(_fromUtf8("menu_file"))
        self.setMenuBar(self.menubar)
        self.statusbar = QtGui.QStatusBar(self)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        self.setStatusBar(self.statusbar)
        self.action_settings = QtGui.QAction(self)
        self.action_settings.setObjectName(_fromUtf8("action_settings"))
        self.action_exit = QtGui.QAction(self)
        self.action_exit.setObjectName(_fromUtf8("action_exit"))
        self.menu_file.addAction(self.action_settings)
        self.menu_file.addAction(self.action_exit)
        self.menubar.addAction(self.menu_file.menuAction())
        self.setup_settings_ui()

    def set_tree_view_model(self, tree_view, header_name):
        self.tree_view_model = QtGui.QStandardItemModel()
        self.tree_view_model.setColumnCount(1)
        self.tree_view_model.setHeaderData(0, QtCore.Qt.Horizontal, header_name)
        tree_view.setModel(self.tree_view_model)

    def setup_settings_ui(self):
        self.settings_dialog = QDialog()
        self.settings_dialog.resize(237, 215)

        self.settings_ok_button = QPushButton("ok", self.settings_dialog)
        self.settings_ok_button.setGeometry(QtCore.QRect(10, 180, 75, 23))

        self.settings_cancel_button = QPushButton("cancel", self.settings_dialog)
        self.settings_cancel_button.setGeometry(QtCore.QRect(150, 180, 75, 23))

        self.settings_label = QtGui.QLabel(self.settings_dialog)
        self.settings_label.setGeometry(QtCore.QRect(10, 30, 211, 21))
        font = QtGui.QFont()
        font.setPointSize(10)
        self.settings_label.setFont(font)

        self.settings_lineEdit = QtGui.QLineEdit(self.settings_dialog)
        self.settings_lineEdit.setGeometry(QtCore.QRect(10, 60, 211, 21))

        self.settings_browse_button = QtGui.QPushButton('browse', self.settings_dialog)
        self.settings_browse_button.setGeometry(QtCore.QRect(10, 90, 75, 23))

        self.settings_dialog.setWindowTitle("Settings")
        self.settings_dialog.setWindowModality(QtCore.Qt.ApplicationModal)

    def retranslate_ui(self):
        self.setWindowTitle(_translate("self", "MPlayer", None))
        self.btn_play.setText(_translate("self", ">", None))
        self.btn_prev.setText(_translate("self", "|<<", None))
        self.btn_stop.setText(_translate("self", "[]", None))
        self.btn_next.setText(_translate("self", ">>|", None))
        self.btn_shuffle.setText(_translate("self", ";;", None))
        self.menu_file.setTitle(_translate("self", "File", None))
        self.action_settings.setText(_translate("self", "Settings", None))
        self.action_exit.setText(_translate("self", "Exit", None))
        self.retranslate_settings_ui()

    def retranslate_settings_ui(self):
        self.settings_ok_button.setText(_translate("Form", "Ok", None))
        self.settings_cancel_button.setText(_translate("Form", "Cancel", None))
        self.settings_label.setText(_translate("Form", "Set music directory:", None))
        self.settings_browse_button.setText(_translate("Form", "Browse", None))

    def quit(self):
        os._exit(0) # without this application freezes.

    def closeEvent(self, event):
        # called when X pressed
        os._exit(0)

class Controller(object):
    def __init__(self, ui, business):
        self.vlc_instance = vlc.Instance()
        self.player = self.vlc_instance.media_player_new()
        self.ui = ui
        self.business = business
        self.program_buttons(ui)
        self.populate_music_tree(self.ui.tree_view_model, self.business.get_music_tree())
        self.ui.settings_lineEdit.setText(business.music_path)
        self.prev_media_index_list = []
        self.current_media_index = None
        self.clicked_on_index = None

    def set_media_for_player(self, media):
        self.player.set_media(media)

    def program_buttons(self, ui):
        ui.action_exit.triggered.connect(ui.quit)
        ui.action_settings.triggered.connect(ui.settings_dialog.exec_)
        ui.connect(ui.btn_play, QtCore.SIGNAL('clicked()'), self.click_on_play)
        ui.connect(ui.btn_stop, QtCore.SIGNAL('clicked()'), self.click_on_stop)
        ui.connect(ui.btn_prev, QtCore.SIGNAL('clicked()'), self.click_on_prev)
        ui.connect(ui.btn_next, QtCore.SIGNAL('clicked()'), self.click_on_next)
        ui.connect(ui.btn_shuffle, QtCore.SIGNAL('clicked()'), self.click_on_shuffle)
        ui.connect(ui.settings_ok_button, QtCore.SIGNAL('clicked()'), self.click_on_settings_ok)
        ui.connect(ui.settings_browse_button, QtCore.SIGNAL('clicked()'), self.browse_button)
        ui.connect(ui.settings_cancel_button, QtCore.SIGNAL('clicked()'), ui.settings_dialog.close)
        ui.tree_view.doubleClicked.connect(self.double_click_on_tree_view_item)
        ui.list_widget.doubleClicked.connect(self.double_click_on_list_widget_item)
        ui.list_widget.pressed.connect(self.click_on_list_widget_item)
        #ui.position_slider.valueChanged.connect(self.position_changed)
        ui.volume_slider.valueChanged.connect(self.volume_changed)
        ui.volume_slider.setValue(100)
        ui.btn_shuffle.setCheckable(True)
        ui.search_line.textChanged.connect(self.search_text_changed)
        QtGui.QShortcut(QtGui.QKeySequence(QtCore.Qt.Key_Delete), ui, self.pushed_delete_key)

    @property
    def current_media_index(self):
        return self._current_media_index if self.ui.list_widget.count() > 1 else None

    @current_media_index.setter
    def current_media_index(self, value):
        with thread_lock:
            if value is not None and value >= 0:
                self.set_item_background_by_index(self.current_media_index, '#ffffff')
                self._current_media_index = value
                self.set_item_background_by_index(self._current_media_index, '#d3d3d3')
            else:
                print('current_media_index.setter: given value not valid for playlist: %s' % value)
                self._current_media_index = None

    def set_item_background_by_index(self, index, color):
        if index and self.ui.list_widget.item(index):
            self.ui.list_widget.item(index).setBackground(QtGui.QColor(color))

    def search_text_changed(self):
        input = unicode(self.ui.search_line.text())
        if input:
            business.music_condition = lambda file_name: input in file_name.lower()
            self.recreate_music_tree()
            self.ui.tree_view.expandAll()
        else:
            business.music_condition = lambda file_name: True
            self.recreate_music_tree()
            self.ui.tree_view.collapseAll()

    def pushed_delete_key(self):
        if self.ui.list_widget.hasFocus():
            items = self.ui.list_widget.selectedItems()
            if self.ui.list_widget.item(self.current_media_index) in items:
                self.current_media_index = None
            if items:
                for item in items:
                    self.ui.list_widget.takeItem(self.ui.list_widget.row(item))

    def drop_listwidget_item(self, urls_iter):
        self.handle_outside_drags(urls_iter)
        item = self.ui.list_widget.currentItem()
        index = self.ui.list_widget.indexFromItem(item).row()
        if self.clicked_on_index == self.current_media_index:
            self.current_media_index = index

    def handle_outside_drags(self, path_iter):
        def add_media_in_path(path):
            if business.is_media_file(path):
                self.add_tree_item_to_playlist(Business.get_songname_from_path(path).decode('utf-8'), path)
            else:
                for media_path in os.listdir(path):
                    add_media_in_path(os.path.join(path, media_path))
        for path in path_iter:
            add_media_in_path(path)

    def click_on_play(self):
        if self.ui.btn_play.text() == "||":
            self.player.pause()
            self.ui.btn_play.setText(_translate("self", ">", None))
        else:
            self.play()

    def click_on_shuffle(self):
        print 'shuffle: ', self.ui.btn_shuffle.isChecked()

    def click_on_prev(self):
        prev_index = self.get_prev_media_index()
        # print 'prev_list', self.prev_media_index_list
        if prev_index is not None:
            self.play_media_by_list_index(prev_index)
        else:
            print('There is no previous music')

    def click_on_next(self):
        next_index = self.get_next_media_index()
        if next_index is not None:
            self.prev_media_index_list.append(self.current_media_index)
            self.play_media_by_list_index(next_index)
        else:
            print('No music comes next')

    def click_on_stop(self):
        self.player.stop()
        self.player.set_position(0.0)
        self.ui.position_slider.setValue(0)
        self.ui.btn_play.setText(_translate("self", ">", None))

    def click_on_settings_ok(self):
        self.change_music_path()
        self.ui.settings_dialog.close()

    def change_music_path(self):
        self.business.music_path = unicode(self.ui.settings_lineEdit.text())
        self.recreate_music_tree()

    def recreate_music_tree(self):
        self.ui.tree_view_model.clear()
        self.ui.set_tree_view_model(self.ui.tree_view, self.business.music_path)
        self.populate_music_tree(self.ui.tree_view_model, self.business.get_music_tree())

    def browse_button(self):
        dir = str(QFileDialog.getExistingDirectory(self.ui.settings_dialog, "Select Directory", self.business.music_path))
        self.ui.settings_lineEdit.setText(dir)

    def play(self):
        self.volume_changed()
        self.player.play()
        time.sleep(0.1)
        play_func = self.get_play_func_by_state()
        if callable(play_func):
            play_func() 
        else:
            print(play_func)

    def play_media_by_list_index(self, index):
        self.ui.list_widget.setCurrentRow(index)
        media_to_play = self.ui.list_widget.item(index).media
        if media_to_play:
            self.current_media_index = index
            self.play_media(media_to_play)

    def play_media_by_item(self, item):
        index = self.ui.list_widget.indexFromItem(item).row()
        self.prev_media_index_list.append(self.current_media_index)
        if index is not None:
            self.play_media_by_list_index(index)
        else:
            print('cannot play by index')

    def get_prev_media_index(self):
        if len(self.prev_media_index_list) > 0 and self.ui.btn_shuffle.isChecked():
            return self.prev_media_index_list.pop()
        elif self.current_media_index != None:
            if self.current_media_index > 0:
                return self.current_media_index - 1
            else:
                return self.ui.list_widget.count() - 1

    def get_next_media_index(self):
        playlist_length = self.ui.list_widget.count()
        if playlist_length:
            if self.ui.btn_shuffle.isChecked():
                return random.randint(0, playlist_length - 1)
            elif self.current_media_index != None and self.current_media_index < playlist_length - 1:
                return self.current_media_index + 1
            else:
                return 0
        else:
            print('playlist is empty')

    def get_play_func_by_state(self):
        return {
                vlc.State.Playing: self.play_state_playing,
                vlc.State.NothingSpecial: self.play_state_nothing,
                vlc.State.Ended: self.play_state_ended
            }.get(self.player.get_state(), 'cannot start music\nstate: %s' % self.player.get_state())

    def play_state_playing(self):
        self.ui.position_slider.setValue(0)
        thread_call(self.change_position_slider_at_playing)
        self.ui.btn_play.setText(_translate("self", "||", None))

    def play_state_nothing(self):
        print('Nothing to play')
        number_of_items = self.ui.list_widget.count()
        if number_of_items >= 1:
            if self.ui.btn_shuffle.isChecked():
                media_index = random.randint(0, number_of_items - 1)
            else:
                media_index = 0
            self.play_media_by_list_index(media_index)

    def play_state_ended(self):
        next_media_index = self.get_next_media_index()
        if next_media_index:
            self.play_media_by_list_index(next_media_index)
        else:
            self.click_on_stop()

    def position_changed(self):
        if app.mouseButtons() == QtCore.Qt.LeftButton:
            self.player.set_position(float(self.ui.position_slider.value()) / 100.0)

    def volume_changed(self):
        self.player.audio_set_volume(int(self.ui.volume_slider.value()))

    def populate_music_tree(self, parent_model, data):
        if data:
            for element_type, element in data.iteritems():
                if element_type == 'files':
                    for file_path in element:
                        item = QStandardItem(Business.get_songname_from_path(file_path))
                        item.type = 'file'
                        self.add_item_to_parent_model(parent_model, item, file_path)
                else:
                    item = QStandardItem(element_type)
                    item.type = 'dir'
                    self.add_item_to_parent_model(parent_model, item, element_type)
                    self.populate_music_tree(item, element)

    def add_item_to_parent_model(self, parent_model, item, path):
        item.path = path # TODO PATH
        item.setEditable(False)
        parent_model.appendRow(item)

    def mid_click_on_tree_view_item(self, model_index):
        model = model_index.model()
        if model:
            item =  model.itemFromIndex(model_index)
            self.go_over_on_tree_view_items(item)

    def go_over_on_tree_view_items(self, item):
        if item.type == 'file':
            self.add_tree_item_to_playlist(unicode(item.text()), item.path)
        elif item.type == 'dir':
            for row in xrange(item.rowCount()):
                self.go_over_on_tree_view_items(item.child(row))

    def double_click_on_tree_view_item(self, index):
        tree_item = index.model().itemFromIndex(index)
        if tree_item.type == 'file':
            list_item = self.add_tree_item_to_playlist(unicode(tree_item.text()), tree_item.path)
            self.ui.list_widget.clearSelection() 
            self.play_media_by_item(list_item)

    def add_tree_item_to_playlist(self, text, path):
        list_widget_item = QListWidgetItem(text)
        list_widget_item.media = self.vlc_instance.media_new(path)
        list_widget_item.media.title = text
        for index in range(self.ui.list_widget.count()):
            if self.ui.list_widget.item(index).media.title == list_widget_item.media.title:
                return self.ui.list_widget.item(index)
        else:
            self.ui.list_widget.addItem(list_widget_item)
        return list_widget_item

    def click_on_list_widget_item(self):
        item = self.ui.list_widget.selectedItems()[0]
        self.clicked_on_index = self.ui.list_widget.indexFromItem(item).row()

    def double_click_on_list_widget_item(self):
        item = self.ui.list_widget.selectedItems()[0]
        self.play_media_by_item(item)
       
    def play_media(self, media):
        self.set_media_for_player(media)
        self.ui.lbl_played_song.setText(media.title)
        self.ui.lbl_media_time.setText('00:00')
        self.player.set_position(0.0)
        self.play()
        self.ui.lbl_media_time_length.setText(time_convert(self.player.get_length()))

    def change_position_slider_at_playing(self):
        while self.player.get_state() == vlc.State.Playing:
            with thread_lock:
                self.ui.position_slider.setValue(self.player.get_position() * 100.0)
                self.ui.lbl_media_time.setText(time_convert(self.player.get_time()))
            time.sleep(0.5)
        if self.player.get_state() == vlc.State.Ended:
            self.play()

class Business(object):
    music_extensions = ('mp3', 'flac')
    # music_path = u'c:\\Stuff\\audio\\'
    music_path = u'q:\\Music\\'
    serialized_data_file = 'music_data.dat'

    @staticmethod
    def get_songname_from_path(file_path):
        file_name = os.path.split(file_path)[-1]
        return file_name[:file_name.rfind('.')]

    def __init__(self):
        self.music_condition = lambda file_name: True

    def get_music_tree(self):
        tree_dict = None
        #tree_list = self.get_serialized_tree()
        if not tree_dict:
            if os.path.exists(self.music_path):
                tree_dict = self.set_up_tree(self.music_path)
            else:
                print('given music path not valid')
        return tree_dict

    def set_up_tree(self, music_path):
        '''
        data: {'files': ['01.Astrix - Poison.mp3', '05.Astrix - Artcore.mp3', '06.Astrix - On Fire.mp3', '08.Astrix - Sex Style.mp3', '09.Astrix - Beyond The Senses.mp3'], 'astrix': {'DummyFolder': {'files': ['06.Astrix - On Fire.mp3', 'dummymp3.mp3'], 'folder2': {'files': ['06.Astrix - On Fire.mp3']}, 'folder3': {'files': ['dummymp3.mp3']}}}}
        '''
        tree_dict = MyDict()
        num_of_sep = sum(1 for i in music_path.split(os.sep) if i)
        for path, folders, files in os.walk(music_path):
            if any(i for i in files if self.music_condition(i)) or self.music_condition(path):
                if path == music_path:
                    self.fill_up_tree_dict(tree_dict, path, files)
                else:
                    folder_keys = path.split(os.sep)[num_of_sep:]
                    self.fill_up_tree_dict(eval('tree_dict' + '["' + '"]["'.join(folder_keys) + '"]'), path, files)
        return tree_dict

    def fill_up_tree_dict(self, tree_dict, path, files):
        tree_dict['files'] = []
        for file in files:
            if self.is_media_file(file) and (self.music_condition(file) or self.music_condition(path)):
                tree_dict['files'].append(os.path.join(path, file))

    def is_media_file(self, file_name):
        return any(file_name.endswith(ext) for ext in self.music_extensions)

if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    ui = UiMainWindow()
    business = Business()
    controller = Controller(ui, business)
    ui.show()
    sys.exit(app.exec_())