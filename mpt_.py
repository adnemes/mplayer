from tkinter import *
from tkinter import ttk, filedialog
import sys, os, vlc, time, random, re, threading, collections

window_height = 600
window_width = 870
music_tree_column_width = 430
music_tree_column_str_widthlimit = 10
tree_column_step_with = 6
playlist_column_width = 370
playlist_column_str_widthlimit = 64

thread_lock = threading.Lock()

def thread_call(func, *args, **kwargs):
    thrd = threading.Thread(target = func, args = args, kwargs = kwargs)
    thrd.start()

def time_convert(ms_time):
    sec_time = ms_time / 1000.0
    min = int(sec_time // 60)
    sec = int((sec_time / 60.0 - min) * 60)
    return '{:0>2}:{:0>2}'.format(min, sec)

class MyDict(dict):
    def __getitem__(self, x):
        try:
            return super(MyDict, self).__getitem__(x)
        except KeyError as e:
            super(MyDict, self).__setitem__(x, MyDict())
            return super(MyDict, self).__getitem__(x)

# class Treeview(ttk.Treeview):

    # def __init__(self, master = None, **kwargs):
        # ttk.Scale.__init__(self, master, **kwargs)

    # def click_mouse1_double(self, event):
        # item = self.focus()
        # print('mouse1 item: %s even: %s' % (item, event))
        # return item

    # def click_mouse2(self, event):
        # iid = self.identify_row(event.y)
        # if iid:
            # # mouse pointer over item
            # self.selection_set(iid)
            # item = self.identify('item', event.x, event.y)
            # print('mouse2 item: %s' % item)
            # return item

# class ListBox(Listbox):
    # pass

class Scale(ttk.Scale):
    """a type of Scale where the left click is hijacked to work like a right click"""
    def __init__(self, master = None, **kwargs):
        ttk.Scale.__init__(self, master, **kwargs)
        # self.bind('<Button-1>', self.set_value)
        self.button_pressed = False
        self.bind('<ButtonRelease-1>', self.set_value)
        self.bind('<ButtonPress-1>', lambda event: setattr(self, 'button_pressed', True))

    def set_value(self, event):
        self.button_pressed = False
        self.event_generate('<Button-3>', x = event.x, y =event.y)
        controller.position_changed()
        controller.volume_changed()

class Frame():
    def __init__(self):
        self.root = Tk()
        self.root.geometry('%sx%s' % (window_width, window_height))
        self.root.resizable(0,0)
        self.root.wm_title('MPT')
        #self.actioner = Actioner(self)
        self.create_element_on_root()
        #self.root.mainloop()

    def create_element_on_root(self):
        self.create_menu()
        self.create_tree()
        self.create_playlist()
        self.create_filter_entry()
        self.create_buttons()
        self.create_sliders()

    def create_menu(self):
        self.menubar = Menu(self.root)
        self.filemenu = Menu(self.menubar, tearoff=0)
        self.menubar.add_cascade(label='File', menu = self.filemenu)
        self.filemenu.add_command(label='Settings', command = self.click_on_settings)
        self.filemenu.add_command(label='Exit', command = self.root.destroy)
        self.root.config(menu=self.menubar)

    def create_tree(self):
        self.music_tree = ttk.Treeview(self.root, show = 'tree')
        # self.music_tree = ttk.Treeview(self.root)
        self.music_tree.column("#0", width = music_tree_column_width)
        self.music_tree.place(x = 10, y = 10, width = 435, height = 500)
        self.music_tree_yScroll = Scrollbar(self.root, orient = 'vertical', command = self.music_tree.yview)
        self.music_tree_yScroll.place(x = 445, y = 10, width = 16, height = 500)
        self.music_tree.configure(yscrollcommand = self.music_tree_yScroll.set)

        self.music_tree_xScroll = Scrollbar(self.root, orient = 'horizontal', command = self.music_tree.xview)
        self.music_tree_xScroll.place(x = 10, y = 510, width = 451, height = 16)
        self.music_tree.configure(xscrollcommand = self.music_tree_xScroll.set)

    def create_playlist(self):
        self.playlist = ttk.Treeview(self.root, show = 'tree')
        self.playlist.column("#0", width = playlist_column_width)
        self.playlist.place(x = 470, y = 260, width = 371, height = 280)
        self.playlist_Scroll = Scrollbar(self.root, orient = 'vertical', command = self.playlist.yview)
        self.playlist_Scroll.place(x = 842, y = 260, width = 16, height = 280)
        self.playlist.configure(yscrollcommand = self.playlist_Scroll.set)

        self.playlist_xScroll = Scrollbar(self.root, orient = 'horizontal', command = self.playlist.xview)
        self.playlist_xScroll.place(x = 470, y = 540, width = 387, height = 16)
        self.playlist.configure(xscrollcommand = self.playlist_xScroll.set)

    def create_filter_entry(self):
        self.filter_entry = Entry(self.root, bd = 2)
        self.filter_entry.place(x = 10, y = 540, width = 451, height = 21)
        self.filter_entry.focus_set()

    def create_buttons(self):
        self.btn_play = Button(self.root, text = '>')
        self.btn_play.place(x = 470, y = 70, width = 51, height = 51)
        self.btn_prev = Button(self.root, text = '|<<')
        self.btn_prev.place(x = 540, y = 70, width = 51, height = 51)
        self.btn_stop = Button(self.root, text = '[]')
        self.btn_stop.place(x = 610, y = 70, width = 51, height = 51)
        self.btn_next = Button(self.root, text = '>>|')
        self.btn_next.place(x = 680, y = 70, width = 51, height = 51)
        self.lbl_played_song = Label(self.root, text = 'No song is being played', font = ('Tahoma', 14), anchor = 'w')
        self.lbl_played_song.place(x = 470, y = 15, width = 360, height = 30)
        self.btn_shuffle = Button(self.root, text = ';;')
        self.btn_shuffle.place(x = 750, y = 70, width = 51, height = 51)

    def create_sliders(self):
        self.volume_slider = Scale(self.root, from_ = 100, to = 0, orient = 'vertical')
        self.volume_slider.place(x = 817, y = 70, width = 50, height = 101)
        self.volume_slider.set(100)
        self.position_slider = Scale(self.root, to = 100)
        self.position_slider.place(x = 470, y = 180, width = 385, height = 50)
        self.lbl_media_time_length = Label(self.root)
        self.lbl_media_time_length.place(x = 818, y = 220, width = 40, height = 30)
        self.lbl_media_time = Label(self.root)
        self.lbl_media_time.place(x = 470, y = 220, width = 40, height = 30)

    def click_on_settings(self):
        self.settings = Tk()
        self.settings.geometry('%sx%s' % (310, 150))
        self.settings.resizable(0,0)
        self.settings.wm_title('Settings')
        self.settings_ok_button = Button(self.settings, text = 'ok')
        self.settings_ok_button.place(x = 10, y = 100, width = 75, height = 23)
        self.settings_cancel_button = Button(self.settings, text = 'cancel', command = self.settings.destroy)
        self.settings_cancel_button.place(x = 225, y = 100, width = 75, height = 23)
        self.settings_label = Label(self.settings, text = 'Set music directory', font = 'Tahoma')
        self.settings_label.place(x = 10, y = 30, width = 211, height = 21)
        self.entry_music_path = Entry(self.settings, bd = 2)
        self.entry_music_path.place(x = 10, y = 60, width = 211, height = 21)
        self.settings_browse_button = Button(self.settings, text = 'browse')
        self.settings_browse_button.place(x = 225, y= 60, width = 75, height = 23)
        controller.program_settings()

class Controller():
    def __init__(self, ui, business):
        self.vlc_instance = vlc.Instance()
        self.player = self.vlc_instance.media_player_new()
        self.ui = ui
        self.business = business
        self.filter_text = StringVar()
        self.filter_text.trace_add("write", self.filter_text_changed)
        self.program_buttons(ui)
        self.shuffle = False
        self.folder_counter = 0
        self.populate_music_tree(self.business.get_music_tree())
        #self.ui.settings_lineEdit.setText(business.music_path)
        self.prev_media_list = []
        self.current_media_path = None
        self.playlist_width = playlist_column_width

    def is_media(self, item):
        return 'folder' not in item and os.path.isfile(item)

    def program_settings(self):
        self.ui.entry_music_path.delete(0, END)
        self.ui.entry_music_path.insert(0, self.business.music_path)
        self.ui.settings_browse_button.config(command = self.click_on_file_browse)
        self.ui.settings_ok_button.config(command = self.click_on_settings_ok)

    def click_on_file_browse(self):
        file_path = filedialog.askdirectory(initialdir = business.music_path)
        self.ui.entry_music_path.delete(0, END)
        self.ui.entry_music_path.insert(0, file_path)
        self.ui.settings.focus_force()

    def program_buttons(self, ui):
        ui.btn_play.config(command = self.click_on_play)
        ui.btn_prev.config(command = self.click_on_prev)
        ui.btn_stop.config(command = self.click_on_stop)
        ui.btn_next.config(command = self.click_on_next)
        ui.btn_shuffle.config(command = self.click_on_shuffle)
        ui.music_tree.bind('<Double-1>', lambda event: self.double_click_on_tree(event, ui.music_tree))
        ui.music_tree.bind('<Button-2>', self.mid_click_on_tree_view_item)
        ui.playlist.bind('<Double-1>', lambda event: self.double_click_on_tree(event, ui.playlist))
        ui.playlist.bind('<Delete>', self.delete_selected_items_from_playlist)
        ui.playlist.bind('<ButtonPress-1>', self.set_playlist_selection)
        ui.playlist.bind('<ButtonRelease-1>', lambda event: self.set_playlist_selection(event, invert = True))
        ui.playlist.bind('<B1-Motion>', self.move_playlist_selection)
        ui.filter_entry.config(textvariable = self.filter_text)
        ui.root.protocol('WM_DELETE_WINDOW',lambda: (ui.root.destroy(), self.player.stop()))
        ui.playlist.bind("<Control-a>", self.select_all_in_playlist)


    def filter_text_changed(self, *args, **kwargs):
        input = self.filter_text.get()
        if input:
            business.music_condition = lambda file_name: input in file_name.lower()
            self.recreate_music_tree()
        else:
            business.music_condition = lambda file_name: True
            self.recreate_music_tree()

    def click_on_play(self):
        if self.ui.btn_play.cget('text') == "||":
            self.player.pause()
            self.ui.btn_play.config(text = ">")
        else:
            self.play()

    def click_on_shuffle(self):
        if self.shuffle == True:
            self.ui.btn_shuffle.configure(relief = RAISED)
            self.shuffle = False
        else:
            self.ui.btn_shuffle.configure(relief = SUNKEN)
            self.shuffle = True

    def click_on_prev(self):
        if self.current_media_path:
            if self.shuffle == True and self.prev_media_list:
                prev_path = self.prev_media_list.pop()
            else:
                prev_path = self.ui.playlist.prev(self.current_media_path)
            if prev_path:
                self.play_media(prev_path, save_prev = False)
                return True
            else:
                print('There is no previous music')

    def click_on_next(self):
        if self.current_media_path:
            if self.shuffle == True:
                next_path = self.get_next_randomly()
            else:
                print('self.current_media_path: ', self.current_media_path)
                next_path = self.ui.playlist.next(self.current_media_path)
                print('next_path: ', next_path)
            if next_path:
                self.play_media(next_path)
                return True
            else:
                print('No music comes next')

    def get_next_randomly(self):
        chosen = None
        items = self.ui.playlist.get_children()
        prev_counter = len(items) / 3 * 2
        while chosen is None:
            media = items[random.randint(0, len(items) - 1)]
            if media != self.current_media_path and (media not in self.prev_media_list or prev_counter <= 0):
                chosen = media
            else:
                prev_counter -= 1
        return chosen

    def click_on_stop(self):
        self.player.stop()
        self.player.set_position(0.0)
        self.ui.position_slider.set(0)
        self.ui.lbl_media_time['text'] = '00:00'
        self.ui.btn_play['text'] = ">"

    def click_on_settings_ok(self):
        self.change_music_path()
        self.ui.settings.destroy()

    def change_music_path(self):
        self.business.music_path = self.ui.entry_music_path.get()
        self.recreate_music_tree()

    def recreate_music_tree(self):
        self.ui.music_tree.delete(*self.ui.music_tree.get_children())
        self.populate_music_tree(self.business.get_music_tree())

    def play(self):
        self.volume_changed()
        self.player.play()
        time.sleep(0.1)
        play_func = self.get_play_func_by_state()
        if callable(play_func):
            play_func() 
        else:
            print(play_func)

    def get_play_func_by_state(self):
        return {
                vlc.State.Playing: self.play_state_playing,
                vlc.State.NothingSpecial: self.play_state_nothing,
                vlc.State.Ended: self.play_state_ended
            }.get(self.player.get_state(), 'cannot start music\nstate: %s' % self.player.get_state())

    def play_state_playing(self):
        self.ui.position_slider.set(0)
        thread_call(self.change_widgets_at_playing)
        self.ui.btn_play.config(text = "||")

    def play_state_nothing(self):
        items = self.ui.playlist.get_children()
        if len(items) > 0:
            self.play_media(items[random.randint(0, len(items) - 1) if self.shuffle else 0])
        else:
            print('Nothing to play')

    def play_state_ended(self):
        if not self.click_on_next():
            self.click_on_stop()

    def position_changed(self):
        self.player.set_position(float(self.ui.position_slider.get()) / 100.0)

    def volume_changed(self):
        self.player.audio_set_volume(int(self.ui.volume_slider.get()))

    def populate_music_tree(self, data, parent = '', str_length = 0):
        if str_length == 0:
            self.music_tree_width = music_tree_column_width
        if data:
            for element_type, element in data.items():
                if element_type == 'files':
                    for file_path in element:
                        media_name = Business.get_songname_from_path(file_path)
                        self.ui.music_tree.insert(parent , 'end', file_path, text = media_name)
                        self.do_music_tree_scroll_scretch(media_name, str_length)
                else:
                    folder_name = self.create_folder_name(element_type)
                    self.ui.music_tree.insert(parent, 'end', folder_name, text = element_type)
                    if self.filter_text.get():
                        self.ui.music_tree.item(folder_name, open = True)
                    self.populate_music_tree(element, folder_name, str_length + len(element_type))

    def do_music_tree_scroll_scretch(self, media_name, str_length):
        new_music_tree_new_width = (str_length + len(media_name) - music_tree_column_str_widthlimit) * tree_column_step_with
        if new_music_tree_new_width > self.music_tree_width:
            self.music_tree_width = new_music_tree_new_width
        #print('music_tree_new_width: %s' % self.music_tree_width)
        self.ui.music_tree.column("#0", width = self.music_tree_width)

    def create_folder_name(self, name):
        self.folder_counter += 1
        return 'folder_' + ''.join(s for s in name if s.isdigit() or s in [chr(i) for i in range(65, 124)]) + str(self.folder_counter)

    def mid_click_on_tree_view_item(self, event):
        iid = self.ui.music_tree.identify_row(event.y)
        if iid:
            self.ui.music_tree.selection_set(iid)
            item = self.ui.music_tree.identify('item', event.x, event.y)
            print('mouse2 item: %s' % item)
            self.go_over_on_tree_view_items(item)

    def go_over_on_tree_view_items(self, item):
        if self.is_media(item):
            self.add_tree_item_to_playlist(item)
        else:
            children = self.ui.music_tree.get_children([item])
            if children:
                for child_item in children:
                    self.go_over_on_tree_view_items(child_item)

    def double_click_on_tree(self, event, tree):
        #item = tree.focus()
        item = tree.identify('item', event.x, event.y)
        #print('mouse1 item: %s even: %s' % (item, event))
        if self.is_media(item):
            self.add_tree_item_to_playlist(item)
            self.ui.playlist.selection_set(item)
            self.play_media(item)

    def double_click_on_tree_view_item(self, event):
        self.double_click_on_tree(event, self.ui.music_tree)

    def add_tree_item_to_playlist(self, file_path):
        if not file_path in self.ui.playlist.get_children():
            song_name = Business.get_songname_from_path(file_path)
            self.ui.playlist.insert('', END, file_path, text = song_name, tags = 'off')
            if len(song_name) * tree_column_step_with > self.playlist_width:
                self.playlist_width = len(song_name) * tree_column_step_with
        self.ui.playlist.column("#0", width = self.playlist_width)


    def play_media(self, path, save_prev = True):
        print('play_media: ', path)
        media = self.vlc_instance.media_new(path)
        self.player.set_media(media)
        self.ui.playlist.selection_set(path)
        if save_prev:
            self.prev_media_list.append(self.current_media_path)
        self.current_media_path = path
        self.set_played_item_color(self.ui.playlist, self.current_media_path)
        self.ui.lbl_played_song['text'] = Business.get_songname_from_path(path)
        self.ui.lbl_media_time['text'] = '00:00'
        self.player.set_position(0.0)
        self.play()
        self.ui.lbl_media_time_length['text'] = time_convert(self.player.get_length())

    def set_played_item_color(self, playlist, id):
        for item in self.ui.playlist.get_children():
            if item == id:
                playlist.item(item, tags = 'on')
            else:
                playlist.item(item, tags = 'off')
        playlist.tag_configure('on', background = 'gray')
        playlist.tag_configure('off', background = 'white')

    def change_widgets_at_playing(self):
        start_path = self.current_media_path
        begin_name = Business.get_songname_from_path(self.current_media_path)
        end_name = ''
        step = 50
        while self.player.get_state() == vlc.State.Playing and start_path == self.current_media_path:
            with thread_lock:
                if self.ui.position_slider.button_pressed == False:
                    self.ui.position_slider.set(self.player.get_position() * 100.0)
                    self.ui.lbl_media_time['text'] = time_convert(self.player.get_time())
            if begin_name == '':
                if step > 0:
                    step -= 1
                else:
                    begin_name = Business.get_songname_from_path(self.current_media_path)
                    step = 50
                    end_name = ''
            else:
                end_name += begin_name[0]
                begin_name = begin_name[1:]
            self.ui.lbl_played_song['text'] = begin_name + ' ' * step + end_name
            time.sleep(0.1)
        if self.player.get_state() == vlc.State.Ended:
            self.play()

    def move_playlist_selection(self, event):
        tv = event.widget
        moveto = tv.index(tv.identify_row(event.y))
        for s in tv.selection():
            print('%s will moved to %s' % (s, moveto))
            tv.move(s, '', moveto)

    def set_playlist_selection(self, event, invert = False):
        tv = event.widget
        if (tv.identify_row(event.y) in tv.selection()) ^ invert:
            tv.selection_set(tv.identify_row(event.y))

    def delete_selected_items_from_playlist(self, event):
        print(f'event: {event}')
        selected_items = self.ui.playlist.selection()
        for selected_item in selected_items:
            self.ui.playlist.delete(selected_item)

    def select_all_in_playlist(self, event):
        self.ui.playlist.selection_set(self.ui.playlist.get_children())

class Business():
    music_extensions = ('mp3', 'flac')
    # music_path = u'c:\\Stuff\\audio\\'
    music_path = 'q:\\music\\'
    # music_path = 'q:\\music\\Full Trance Music Mini Pack Vol.9\\'
    # music_path = 'q:\\Temp\\music\\'
    serialized_data_file = 'music_data.dat'

    @staticmethod
    def get_songname_from_path(file_path):
        file_name = os.path.split(file_path)[-1]
        return file_name[:file_name.rfind('.')]

    @staticmethod
    def get_num_from_item(item):
        m = re.match('(\D*)(\d+)', item)
        if m:
            if m.group(1) and m.group(2):
                return (m.group(1), float(m.group(2)))
            elif m.group(1) and not m.group(2):
                return (m.group(1), float('Inf'))
            elif not m.group(1) and m.group(2):
                return ('z' * 1024, float(m.group(2)))
        else:
            return (item, float('Inf'))

    def __init__(self):
        self.music_condition = lambda file_name: True

    def get_music_tree(self):
        tree_dict = None
        #tree_list = self.get_serialized_tree()
        if not tree_dict:
            if os.path.exists(self.music_path):
                tree_dict = self.set_up_tree(self.music_path)
                tree_dict = collections.OrderedDict(sorted(tree_dict.items(), key = lambda x: Business.get_num_from_item(x[0])))
            else:
                print('given music path not valid')
        return tree_dict

    def set_up_tree(self, music_path):
        '''
        data: {'files': ['01.Astrix - Poison.mp3', '05.Astrix - Artcore.mp3', '06.Astrix - On Fire.mp3', '08.Astrix - Sex Style.mp3', '09.Astrix - Beyond The Senses.mp3'], 'astrix': {'DummyFolder': {'files': ['06.Astrix - On Fire.mp3', 'dummymp3.mp3'], 'folder2': {'files': ['06.Astrix - On Fire.mp3']}, 'folder3': {'files': ['dummymp3.mp3']}}}}
        '''
        tree_dict = MyDict()
        num_of_sep = sum(1 for i in music_path.split(os.sep) if i)
        for path, folders, files in os.walk(music_path):
            if any(i for i in files if self.music_condition(i)) or self.music_condition(path):
                if path == music_path:
                    self.fill_up_tree_dict(tree_dict, path, sorted(files, key = lambda x: Business.get_num_from_item(Business.get_songname_from_path(x))))
                else: #that
                    folder_keys = path.split(os.sep)[num_of_sep:]
                    self.fill_up_tree_dict(eval('tree_dict' + '["' + '"]["'.join(folder_keys) + '"]'), path, files)
        return tree_dict

    def fill_up_tree_dict(self, tree_dict, path, files):
        tree_dict['files'] = []
        for file in files:
            if self.is_media_file(file) and (self.music_condition(file) or self.music_condition(path)):
                tree_dict['files'].append(os.path.join(path, file))

    def is_media_file(self, file_name):
        return any(file_name.endswith(ext) for ext in self.music_extensions)

if __name__ == '__main__':
    frame = Frame()
    business = Business()
    controller = Controller(frame, business)
    frame.root.mainloop()
